const { Client, MessageMedia } = require("whatsapp-web.js");
// const client = new Client();
//import qrcode terminal generator
const qrcodeterminal = require("qrcode-terminal");

const qrcode = require("qrcode");

//***import session untuk agar sekali scan saja
const fs = require("fs");
// const { Client, Location, List, Buttons } = require('./index');
const SESSION_FILE_PATH = "./wa-session.json";
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
  sessionCfg = require(SESSION_FILE_PATH);
}

let client = new Client({
  puppeteer: {
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--disable-accelerated-2d-canvas",
      "--no-first-run",
      "--no-zygote",
      "--single-process", // <- this one doesn't works in Windows
      "--disable-gpu",
    ],
  },
  session: sessionCfg,
});
// You can use an existing session and avoid scanning a QR code by adding a "session" object to the client options.
// This object must include WABrowserId, WASecretBundle, WAToken1 and WAToken2.
//***Selesai code import session untuk agar sekali scan saja

//express
require("dotenv").config();
const express = require("express");
const app = express();
const baseApi = "/api/v1/multiservice";
const cors = require("cors");
const { phoneNumberFormatter } = require("./helpers/formatter");
const { body, validationResult } = require("express-validator");

//mongo connect
let db = null;
var MongoClient = require("mongodb").MongoClient;
MongoClient.connect(process.env.MONGO_HOST, function (err, client) {
  if (err) throw err;

  db = client.db("exseongjin");
});
if (db != null) {
  db.collection("hrd_gaji")
    .find()
    .toArray(function (err, result) {
      if (err) throw err;
      console.log(result);
    });
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});

//server
const http = require("http");
const server = http.createServer(app);

//socket io
const socketIO = require("socket.io");
const io = new socketIO.Server(server, {
  allowEIO3: true,
  cors: {
    origin: true,
    credentials: true,
  },
});

//express api
app.get(baseApi, (req, res) => {
  res.status(200).json({
    status: true,
    message: "Hello multi service NGI is here ",
  });
});

client.on("message", (msg) => {
  if (msg.body == "!ping") {
    msg.reply("pong");
  } else if (msg.body == "malam") {
    msg.reply("Evening");
  }
});

io.on("connection", function (socket) {
  socket.on("mountedConnect", function (data) {
    console.log("masuk");
    io.emit("responseMountedConnect", data + " Success");
  });

  //inisialiasais whataaoo webb
  socket.on("initialisationWhatsaapp", function (data) {
    client.initialize();
    io.emit("initialisationWhatsaappResponse", data + " Success");
  });

  // membuat base 64 qr code untuk whataspp scna
  client.on("qr", (qr) => {
    // Generate and scan this code with your phone
    console.log("QR RECEIVED", qr);
    //generate base64 qrcode menjadi qrcode
    qrcode.toDataURL(qr, (err, url) => {
      console.log("receive qr");
      io.emit("qr", url);
      io.emit("message", "Received Qr Code");
    });
  });

  client.on("ready", () => {
    console.log("ready");

    io.emit("message", "Whatsapp Client is ready!");
  });

  if (fs.existsSync(SESSION_FILE_PATH)) {
    io.emit("message", "Whatsapp Client is authenticated!");
    io.emit("authenticated", "Whatsapp Client is authenticated!");
    sessionCfg = require(SESSION_FILE_PATH);
  }

  client.on("authenticated", (session) => {
    console.log("authenticated");
    io.emit("message", "Whatsapp Client is authenticated!");
    io.emit("authenticated", "Whatsapp Client is authenticated!");

    sessionCfg = session;
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on("auth_failure", function (session) {
    io.emit("message", "Auth Failure , restarting....");
  });
  client.on("disconnected", (reason) => {
    io.emit("message", "Whatsapp Disconnected!");
    fs.unlinkSync(SESSION_FILE_PATH, function (err) {
      if (err) return console.log(err);
      console.log("Session File Deleted");
    });
    client.destroy();
    client.initialize();
  });
});

const checkRegisteredNumber = async function (number) {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
};

app.post(
  baseApi + "/send-message",
  [body("number").notEmpty(), body("message").notEmpty()],
  async (req, res) => {
    const errors = validationResult(req).formatWith(({ msg }) => {
      return msg;
    });
    if (!errors.isEmpty()) {
      return res.status(422).json({
        status: false,
        message: errors.mapped(),
      });
    }

    const number = phoneNumberFormatter(req.body.number);
    const message = req.body.message;

    const isRegisteredNumber = await checkRegisteredNumber(number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: "The number is not registered",
      });
    }

    client
      .sendMessage(number, message)
      .then((response) => {
        //ketika berhaisl
        res.status(200).json({
          status: true,
          response: response,
        });
      })
      .catch((err) => {
        res.status(500).json({
          status: false,
          response: err,
        });
      });
  }
);

async function sendMedia(number, caption, data, path) {
  await generatePdf(data, data.id);
  const media = MessageMedia.fromFilePath(
    "./slipgaji/docs/" + data.id + "_doc.pdf"
  );
  // console.log('numb: ', number, "mess: ",caption )
  client
    .sendMessage(number, media, { caption: caption })
    .then((response) => {
      console.log("pesan terikirim");
    })
    .catch((err) => {
      console.log("gagal kirim pesan");
    });
  console.log("selesai");
}

//send media
app.post(baseApi + "/send-media", async (req, res) => {
  const number = phoneNumberFormatter(req.body.number);
  const caption = req.body.caption;
  const media = MessageMedia.fromFilePath("./elon.jpg");

  const isRegisteredNumber = await checkRegisteredNumber(number);
  if (!isRegisteredNumber) {
    return res.status(422).json({
      status: false,
      message: "The number is not registered",
    });
  }

  sendMedia(number, caption);
});

let channel = null;
//rabbitmq
const amqp = require("amqplib/callback_api");
const { response } = require("express");
//1. create connection
const QUEUE = "server";
const QUEUE_NEW_CHANNEL = "JOB ONE BY ONE";
amqp.connect(process.env.RABBITMQ_HOST, (connError, conn) => {
  if (connError) {
    console.log(connError);
    return "error";
  }

  //w. create channel for publish the message
  // biki channel dan api sever eueue
  conn.createChannel((channelErr, channel) => {
    if (channelErr) {
      console.log(channelErr);
    }
    //3 assert qr
    channel.assertQueue(QUEUE, { durable: true });
    console.log("channel queueu has created: ", QUEUE);
    channel.prefetch(1);

    app.post(
      baseApi + "/queue/send-media",
      [
        body("number").notEmpty(),
        body("caption").notEmpty(),
        body("data").notEmpty(),
      ],
      async (req, res) => {
        const errors = validationResult(req).formatWith(({ msg }) => {
          return msg;
        });
        if (!errors.isEmpty()) {
          return res.status(422).json({
            status: false,
            caption: errors.mapped(),
          });
        }

        const number = phoneNumberFormatter(req.body.number);
        const caption = req.body.caption;
        const data = req.body.data;

        const isRegisteredNumber = await checkRegisteredNumber(number);

        if (!isRegisteredNumber) {
          return res.status(422).json({
            status: false,
            message: "The number is not registered",
          });
        }

        let message = [];

        message.push({
          number: number,
          caption: caption,
          data: data,
        });

        // console.log(message)
        channel.sendToQueue(QUEUE, Buffer.from(JSON.stringify(message)), {
          persistent: true,
        });
        return res.status(200).json({
          status: true,
          message: "Success send to queue",
        });
      }
    );
  });

  //bikini channel one byone
  conn.createChannel((channelErr, channel) => {
    if (channelErr) {
      console.log(channelErr);
    }

    let QUEUE_NEW_CHANNEL = "JOB ONE BY ONE";
    console.log("channel queueu has created: ", QUEUE_NEW_CHANNEL);
    channel.assertQueue(QUEUE_NEW_CHANNEL, { durable: true });
    channel.prefetch(1);
  });

  //publuse to eueue(job one by one) & consume queue(server)
  conn.createChannel((channelErr, channel) => {
    if (channelErr) {
      console.log(channelErr);
    }
    // console.log("send: ", QUEUE);

    channel.prefetch(1);
    client.on("ready", () => {
      channel.consume(
        QUEUE,
        async (msg) => {
          let datas = JSON.parse(msg.content);

          for (const e of datas) {
            //publish to channel queue one by one job
            channel.sendToQueue(
              QUEUE_NEW_CHANNEL,
              Buffer.from(JSON.stringify(e)),
              { persistent: true }
            );
            // await sendMedia(e.number, e.caption);
          }

          setTimeout(function () {
            console.log(" [x] Done QUEUE_NEW_CHANANEL CONSUSME");
            channel.ack(msg);
          }, 3 * 1000);
        },
        {
          // manual acknowledgment mode,
          // see ../confirms.html for details
          noAck: false,
        }
      );
    });
  });

  //consume job one by one
  conn.createChannel((channelErr, channel) => {
    if (channelErr) {
      console.log(channelErr);
    }
    // console.log("send: ", QUEUE);
    channel.prefetch(1);
    client.on("ready", () => {
      channel.consume(
        QUEUE_NEW_CHANNEL,
        async (msg) => {
          // const media = MessageMedia.fromFilePath('./elon.jpg');
          let datas = JSON.parse(msg.content.toString());

          await sendMedia(datas.number, datas.caption, datas.data);

          setTimeout(function () {
            console.log(" Done Send to Media");
            channel.ack(msg);
          }, 3 * 1000);
        },
        {
          // manual acknowledgment mode,
          // see ../confirms.html for details
          noAck: false,
        }
      );
    });
  });
});

const fs1 = require("fs");
const pdf = require("pdf-creator-node");
const options = require("./helpers/option");
const path = require("path");
function generatePdf(data, name) {
  const html = fs.readFileSync(
    path.join(__dirname, "./slipgaji/template.html"),
    "utf-8"
  );

  const fileName = name + "_doc.pdf";
  console.log("datasss", data.nama);
  const document = {
    html: html,
    data: data,
    path: "./slipgaji/docs/" + fileName,
  };
  pdf
    .create(document, options)
    .then((res) => {
      console.log("pdfres", res);
    })
    .catch((err) => {
      console.log("pdferror", err);
    });
}

//running express js
server.listen(process.env.APP_PORT, function () {
  console.log("Multiservice is Start : ", process.env.APP_PORT);
});
