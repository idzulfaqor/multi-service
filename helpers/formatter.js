const phoneNumberFormatter = function(number){
    //1. megnghilangkan karakter selain angka 
    let formaatted = number.replace(/\D/g,'')

    //2. menghilangkan angka 0 diganti 62
    if(formaatted.startsWith('0')){
        formaatted  ='62'+formaatted.substr(1);
    }

    //3. jika nomo rtidak mengandung .us diakhir maka ditambahkan
    if(!formaatted.endsWith('@c.us')){
        formaatted += '@c.us'
    }

    return formaatted;


}


module.exports = {
    phoneNumberFormatter
}