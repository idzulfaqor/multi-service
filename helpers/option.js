module.exports = {
        formate:'legal',
        orientation:'potrait',
        border:'8mm',
        
        footer:{
            height:'20mm',
            contents: {
                first:"Cover Page",
                2:'Second Page',
                default:'<span style="color:#444;">{{page}} </span><span>{{pages}} </span>',
                last:'Last Page'
            }
        }
}