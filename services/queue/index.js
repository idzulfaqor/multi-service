const amqp = require("amqplib/callback_api");
const { response } = require("express");

let cons = null;
async function connect(channelData) {
  await amqp.connect(process.env.RABBITMQ_HOST, (err, conn) => {
    if (err) {
      console.log("error Queue");
      return "500";
    }
    cons = conn;
    console.log("Success Connect To Queue");
    if (channelData) {
      if (typeof channelData == "object") {
        //jika sudah connect maka consume queue yang tersedia

        conn.createChannel((channelErr, channel) => {
          if (channelErr) {
            return "503: error channel";
          }

          //make channel first here
          for (let i = 0; i < channelData.length; i++) {
            channel.assertQueue(channelData[i], { durable: true });
            channel.prefetch(1);
            console.log("channel queueu has created: ", channelData[i]);
          }

          for (let i = 0; i < channelData.length; i++) {
            //ini consume
            channel.prefetch(1);
            channel.consume(
              channelData[i],
              async (msg) => {
                console.log("sukses consume " + channelData[i]);
                console.log(JSON.parse(msg.content.toString()));
                channel.ack(msg);
              },
              {
                // manual acknowledgment mode,
                // see ../confirms.html for details
                noAck: false,
              }
            );
          }
        });
      }
    }
  });
}

//publish message
function sendToQueue(channelName, msg) {
  if (!channelName) {
    return "403";
  }

  cons.createChannel((err, channel) => {
    if (err) {
      return "503";
    }

    channel.assertQueue(channelName, { durable: true });
    channel.prefetch(1);
    channel.sendToQueue(channelName, Buffer.from(JSON.stringify(msg)));
    console.log("Success Send to queue");
    return "200";
  });
}

module.exports = { sendToQueue, connect };
