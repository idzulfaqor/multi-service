const { Client, MessageMedia } = require("whatsapp-web.js");
// const client = new Client();
//import qrcode terminal generator
const qrcodeterminal = require("qrcode-terminal");

const qrcode = require("qrcode");

//***import session untuk agar sekali scan saja
const fs = require("fs");
// const { Client, Location, List, Buttons } = require('./index');
const SESSION_FILE_PATH = "./wa-session.json";
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
  sessionCfg = require(SESSION_FILE_PATH);
}

let client = new Client({
  puppeteer: {
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--disable-accelerated-2d-canvas",
      "--no-first-run",
      "--no-zygote",
      "--single-process", // <- this one doesn't works in Windows
      "--disable-gpu",
    ],
  },
  session: sessionCfg,
});
// You can use an existing session and avoid scanning a QR code by adding a "session" object to the client options.
// This object must include WABrowserId, WASecretBundle, WAToken1 and WAToken2.
//***Selesai code import session untuk agar sekali scan saja

if (fs.existsSync(SESSION_FILE_PATH)) {
  io.emit("message", "Whatsapp Client is authenticated!");
  io.emit("authenticated", "Whatsapp Client is authenticated!");
  sessionCfg = require(SESSION_FILE_PATH);
} else {
  client.initialize();
}

//when there is a message
client.on("message", (msg) => {
  if (msg.body == "!ping") {
    msg.reply("pong");
  } else if (msg.body == "malam") {
    msg.reply("Evening");
  }
});

// membuat base 64 qr code untuk whataspp scna
client.on("qr", (qr) => {
  // Generate and scan this code with your phone
  console.log("QR RECEIVED", qr);
  //generate base64 qrcode menjadi qrcode
  qrcode.toDataURL(qr, (err, url) => {
    console.log("receive qr");
    io.emit("qr", url);
    io.emit("message", "Received Qr Code");
  });
});

client.on("ready", () => {
  console.log("ready");

  io.emit("message", "Whatsapp Client is ready!");
});

client.on("authenticated", (session) => {
  console.log("authenticated");
  io.emit("message", "Whatsapp Client is authenticated!");
  io.emit("authenticated", "Whatsapp Client is authenticated!");

  sessionCfg = session;
  fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
    if (err) {
      console.error(err);
    }
  });
});

client.on("auth_failure", function (session) {
  io.emit("message", "Auth Failure , restarting....");
});
client.on("disconnected", (reason) => {
  io.emit("message", "Whatsapp Disconnected!");
  fs.unlinkSync(SESSION_FILE_PATH, function (err) {
    if (err) return console.log(err);
    console.log("Session File Deleted");
  });
  client.destroy();

  //inisialisasi ulang
  client.initialize();
});

const checkRegisteredNumber = async function (number) {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
};

module.export = checkRegisteredNumber;
