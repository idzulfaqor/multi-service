//express
require("dotenv").config();
const express = require("express");
const app = express();
const baseApi = "/api/v1/multiservice";
const cors = require("cors");
const { phoneNumberFormatter } = require("./helpers/formatter");
const { body, validationResult } = require("express-validator");

//mongo connect
// let db = null;
// var MongoClient = require("mongodb").MongoClient;
// MongoClient.connect(process.env.MONGO_HOST, function (err, client) {
//   if (err) throw err;

//   db = client.db("exseongjin");
// });
// if (db != null) {
//   db.collection("hrd_gaji")
//     .find()
//     .toArray(function (err, result) {
//       if (err) throw err;
//       console.log(result);
//     });
// }

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});

//server
const http = require("http");
const server = http.createServer(app);

//socket io
const socketIO = require("socket.io");
const io = new socketIO.Server(server, {
  allowEIO3: true,
  cors: {
    origin: true,
    credentials: true,
  },
});

//express api
app.get(baseApi, (req, res) => {
  res.status(200).json({
    status: true,
    message: "Hello multi service NGI is here ",
  });
});

io.on("connection", function (socket) {
  socket.on("mountedConnect", function (data) {
    console.log("masuk");
    io.emit("responseMountedConnect", data + " Success");
  });

  //inisialiasais whataaoo webb
  socket.on("initialisationWhatsaapp", function (data) {
    // client.initialize();
    io.emit("initialisationWhatsaappResponse", data + " Success");
  });
});

app.post(
  baseApi + "/send-message",
  [body("number").notEmpty(), body("message").notEmpty()],
  async (req, res) => {
    const errors = validationResult(req).formatWith(({ msg }) => {
      return msg;
    });
    if (!errors.isEmpty()) {
      return res.status(422).json({
        status: false,
        message: errors.mapped(),
      });
    }

    const number = phoneNumberFormatter(req.body.number);
    const message = req.body.message;

    const isRegisteredNumber = await checkRegisteredNumber(number);
    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: "The number is not registered",
      });
    }

    client
      .sendMessage(number, message)
      .then((response) => {
        //ketika berhaisl
        res.status(200).json({
          status: true,
          response: response,
        });
      })
      .catch((err) => {
        res.status(500).json({
          status: false,
          response: err,
        });
      });
  }
);

async function sendMedia(number, caption, data, path) {
  await generatePdf(data, data.id);
  const media = MessageMedia.fromFilePath(
    "./slipgaji/docs/" + data.id + "_doc.pdf"
  );
  // console.log('numb: ', number, "mess: ",caption )
  client
    .sendMessage(number, media, { caption: caption })
    .then((response) => {
      console.log("pesan terikirim");
    })
    .catch((err) => {
      console.log("gagal kirim pesan");
    });
  console.log("selesai");
}

//send media
app.post(baseApi + "/send-media", async (req, res) => {
  const number = phoneNumberFormatter(req.body.number);
  const caption = req.body.caption;
  const media = MessageMedia.fromFilePath("./elon.jpg");

  const isRegisteredNumber = await checkRegisteredNumber(number);
  if (!isRegisteredNumber) {
    return res.status(422).json({
      status: false,
      message: "The number is not registered",
    });
  }

  sendMedia(number, caption);
});

const fs1 = require("fs");
const pdf = require("pdf-creator-node");
const options = require("./helpers/option");
const path = require("path");
function generatePdf(data, name) {
  const html = fs.readFileSync(
    path.join(__dirname, "./slipgaji/template.html"),
    "utf-8"
  );

  const fileName = name + "_doc.pdf";
  console.log("datasss", data.nama);
  const document = {
    html: html,
    data: data,
    path: "./slipgaji/docs/" + fileName,
  };
  pdf
    .create(document, options)
    .then((res) => {
      console.log("pdfres", res);
    })
    .catch((err) => {
      console.log("pdferror", err);
    });
}

const amqp = require("amqplib/callback_api");

//queue load
// const queue = require("./services/queue");
// //connect and send cannel name
// queue.connect(["POS1", "POS2"]);
// setTimeout(() => {
//   let msg = { nama: "andi", umur: "10" };
//   let channel = queue.sendToQueue("POS2", msg);
//   console.log(channel);
// }, 3000);

//running express js
server.listen(process.env.APP_PORT, function () {
  console.log("Multiservice is Start : ", process.env.APP_PORT);
});
